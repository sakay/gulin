<?php
require_once 'cache.php';
$usdt_price = 0;
$cacheTimeSeconds = 10;
ini_set('display_errors',1);

$cryArr = getCryptoBTCValues('BTC', $cacheTimeSeconds);
$polArr = getPoloniexBTCValues('BTC', $cacheTimeSeconds);
$binArr = getBinanceBTCValues('BTC', $cacheTimeSeconds);
//$kraArr = getKrakenBTCValues('XBT', $cacheTimeSeconds);
$mergedArr = array();
$mergedArr = mergeMarkets($mergedArr, $cryArr);
$mergedArr = mergeMarkets($mergedArr, $polArr);
$mergedArr = mergeMarkets($mergedArr, $binArr);

function mergeMarkets($mergedArr, $data) {
	foreach ($data as $key => $val) {
		$mergedArr[$key][$val['market']] = $val;
	}
return $mergedArr;
}

foreach ($mergedArr as $key => $val) {
	if (count($val)< 2) {
	unset($mergedArr[$key]);
	}
}
$compareBTCResult = comparePrices($mergedArr);

$priceDiff = array();
$usdt_price = floatval($usdt_price);
foreach ($compareBTCResult as $key => $row)
{
    $priceDiff[$key] = $row['max'];
}
array_multisort($priceDiff, SORT_DESC, $compareBTCResult);
display($compareBTCResult);

function display($compareBTCResult) {
global $usdt_price; ?>
<!DOCTYPE html>
<html>
<head>
<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 80%;
    
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
</head>
<body>

<table align="center">
  <tr>
    <th>Coin</th>
    <th>From</th>
    <th>To</th>
    <th>Diff (BTC)</th>
    <th>Potential Earning (USDT) w/1BTC</th>
  </tr>
<?php 
	foreach ($compareBTCResult as $coin => $val) { ?>
		<tr>
                  <td><?php echo $coin ?></td>
<?php
		foreach ($val as $key => $mval) {
			if ($key != 'max') {	
				foreach ($mval['diff'] as $fkey => $mval) {
					if ($mval == $val['max'] && $mval <= 0) {echo '<td>0</td>';} 
					else if ($mval == $val['max']) {
    						echo '<td>'.$fkey.'</td>';
    						echo '<td>'.$key.'</td>';
    						echo '<td>'.$mval.'</td>';
						echo '<td>$'.number_format($mval * $usdt_price,2).'</td>';
					}
				}
			}
		}
	?></tr><?php
	}?>
</table>

</body>
</html>


<?
}
function getKrakenBTCValues($baseCurrency, $cacheTimeSeconds) {
$c = new Cache();
$c->eraseExpired();
if (!$c->isCached('kraCache')){
$kraArr = file_get_contents('https://api.kraken.com/0/public/AssetPairs');
$c->store('kraCache', $kraArr,$cacheTimeSeconds);
} else {
$kraArr = $c->retrieve('kraCache');
}
$kraObj = json_decode($kraArr);
echo '<pre>';print_r($kraObj);echo '</pre>';exit;
$kraArr = array();
	foreach ($kraObj as $key => $value) {
                if (strpos($value->symbol, $baseCurrency) !== false){
                $symbol = $value->symbol;
                $symbol = str_replace($baseCurrency, '',$symbol );
                $binArr[$symbol]['ct'] = $baseCurrency;
                $binArr[$symbol]['price'] = $value->price;
                $binArr[$symbol]['market'] = 'Kraken';
        }
}
return $kraArr;
}

function getPoloniexBTCValues($baseCurrency, $cacheTimeSeconds) {
$c = new Cache();
$c->eraseExpired();
if (!$c->isCached('polCache')){
$polArr = file_get_contents('https://poloniex.com/public?command=returnTicker');
$c->store('polCache', $polArr,$cacheTimeSeconds);
} else {
$polArr = $c->retrieve('polCache');
}
$polObj = json_decode($polArr);
$polArr = array();
foreach ($polObj as $key => $value) {
            $breakCur = explode('_',$key);
	if ($breakCur[0] == $baseCurrency) {
             $polArr[$breakCur[1]]['ct'] = $baseCurrency;
             $polArr[$breakCur[1]]['price'] = $value->last;
             $polArr[$breakCur[1]]['market'] = 'Poloniex';
        }
}
return $polArr;
}
function getBinanceBTCValues($baseCurrency, $cacheTimeSeconds) {
global $usdt_price;
$c = new Cache();
$c->eraseExpired();
if (!$c->isCached('binCache')){
$binArr = file_get_contents('https://www.binance.com/api/v3/ticker/price');
$c->store('binCache', $binArr,$cacheTimeSeconds);
} else {
$binArr = $c->retrieve('binCache');
}
$binObj = json_decode($binArr);
$binArr = array();
foreach ($binObj as $key => $value) {
		if ($value->symbol == 'BTCUSDT') {
			$usdt_price = $value->price;
		}
		if (strpos($value->symbol, $baseCurrency) !== false){
		$symbol = $value->symbol;
		$symbol = str_replace($baseCurrency, '',$symbol );
                $binArr[$symbol]['ct'] = $baseCurrency;
                $binArr[$symbol]['price'] = $value->price;
                $binArr[$symbol]['market'] = 'Binance';
		}
}
return $binArr;
}
function getCryptoBTCValues($baseCurrency, $cacheTimeSeconds) {
$c = new Cache();
$c->eraseExpired();
if (!$c->isCached('cryCache')){
$cryArr = file_get_contents('https://www.cryptopia.co.nz/api/GetMarkets/'.$baseCurrency);
$c->store('cryCache', $cryArr,$cacheTimeSeconds);
} else {
$cryArr = $c->retrieve('cryCache');
}
$cryObj = json_decode($cryArr);
$cryArr = array();
foreach ($cryObj->Data as $key => $value) {
	    $breakCur = explode('/',$value->Label);
	$cryArr[$breakCur[0]]['ct'] = $baseCurrency;
	$cryArr[$breakCur[0]]['price'] = number_format($value->LastPrice,8);
	$cryArr[$breakCur[0]]['market'] = 'Cryptopia';
}
return $cryArr;
}

function diffFinder($marketArr, $fromMarket, &$maxDiff) {
$diffArr = array();
	foreach ($marketArr as $key => $val) {
			if ($key != $fromMarket) {
				$price = number_format($marketArr[$fromMarket]['price'] - $val['price'],8);
				$diffArr['diff'][$key] = $price;
				if ( $price > $maxDiff ) {
				$maxDiff = $price;
				}
			}
	}
return $diffArr;
}
function comparePrices($allData) {
	$finalArr = array();
	foreach ($allData as $key => $val) {
		$maxDiff = 0;
		foreach ($val as $mkey => $mval) {
			$diffArr = diffFinder($val, $mkey, $maxDiff);
				$allData[$key][$mkey] = array_merge($allData[$key][$mkey], $diffArr);
				$allData[$key]['max'] = $maxDiff;
		}
	}
	return $allData;
}

?>
